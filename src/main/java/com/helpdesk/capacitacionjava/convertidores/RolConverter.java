/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.convertidores;

import com.helpdesk.capacitacionjava.modelos.Roles;
import com.helpdesk.capacitacionjava.servicios.RolesService;
import javax.ejb.EJB;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author David Ramirez
 */
@FacesConverter("converterRol")
public class RolConverter implements Converter {
    @EJB
    private RolesService rolesService;
    
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if (value != null && value.trim().length() > 0) {
            try{
                Roles rol;
                rol = rolesService.obtenerRol(Integer.valueOf(value));
                return rol;
            }catch(NumberFormatException e){
                System.out.println("Error obteniendo rol :" + e);
            }
        } 
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
        if(object != null) {
            return String.valueOf(((Roles) object).getRolId());
        }
        else {
            return null;
        }
    }
    
}
