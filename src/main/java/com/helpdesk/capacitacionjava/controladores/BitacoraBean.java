/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.controladores;

import com.helpdesk.capacitacionjava.controladores.*;
import com.helpdesk.capacitacionjava.modelos.Bitacora;
import com.helpdesk.capacitacionjava.servicios.BitacoraService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;

/**
 *
 * @author David Ramirez
 */
@Getter
@Setter
@Named(value = "bitacoraBean")
@ViewScoped
public class BitacoraBean implements Serializable {

    /**
     * Creates a new instance of Bitacora
     */
    public BitacoraBean() {
    }
    
    private Bitacora bita;
    private List<Bitacora> todosLosEstados;
    /*sirve para instanciar en ina sola clase*/
    @EJB
    private BitacoraService bitacoraService;

    /*Cargara los objetos antes que lleguen a la vista*/
    @PostConstruct
    public void initBit() {
        bita = new Bitacora();
        todosLosEstados = bitacoraService.listaBitacora();
    }

    public void guardarBitacura() {
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (bita.getBitaId() == null) {
            bitacoraService.guardarBitacora(bita);
            bita = new Bitacora();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Bitacora Guardado", "Nuevo"));
        } else {
            bitacoraService.actualizarBitacora(bita);
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Bitacora Actualizado", "Actualizar"));
            requestContext.execute("PF('dlg_editar_bitacora').hide();");
        }
    }
    
    public void verBitacora(Bitacora bita) {
        this.bita = bita;
    }
    
    public void eliminarBitacora() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (bita.getBitaId()!= 0) {
            bitacoraService.eleminarBitacora(bita);
            bita = new Bitacora();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Bitacora Eliminado", "Eliminar"));
        }

    }

    public void limpiarDialogo() {
        bita = new Bitacora();
    }
    public List<Bitacora> bitacoraslist(){
        return bitacoraService.listaBitacora();
    } 

}
