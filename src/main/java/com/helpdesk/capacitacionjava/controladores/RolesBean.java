/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.controladores;


import com.helpdesk.capacitacionjava.modelos.Roles;

import com.helpdesk.capacitacionjava.servicios.RolesService;
import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author David Ramirez
 */
@Getter
@Setter
@Named(value = "rolesBean")
@ViewScoped
public class RolesBean implements Serializable {

    @EJB
    private RolesService rolesService;
    
    public List<Roles> roleslist() {
        return rolesService.listaRoles();
    }
}
