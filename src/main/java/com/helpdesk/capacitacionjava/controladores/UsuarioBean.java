/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helpdesk.capacitacionjava.controladores;


import com.helpdesk.capacitacionjava.modelos.Roles;
import com.helpdesk.capacitacionjava.modelos.Usuarios;
import com.helpdesk.capacitacionjava.servicios.RolesService;
import com.helpdesk.capacitacionjava.servicios.UsuarioService;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.context.RequestContext;

/**
 *
 * @author David Ramirez
 */
@Getter
@Setter
@Named(value = "usuarioBean")
@ViewScoped
public class UsuarioBean implements Serializable {

    public UsuarioBean() {
    }

    private Usuarios usur;
    private List<Usuarios> todosLosEstados;

    private List<Roles> todoLosRoles;
    private Roles rol;

    @EJB
    private UsuarioService usuarioService;

    @EJB
    private RolesService rolesService;

    @PostConstruct
    public void initUsur() {
        /*usur = objeto instancia el modelo Usuarios */
        usur = new Usuarios();
        rol = new Roles();
        usur.setRoles(rol);
        todoLosRoles = new ArrayList<>();
        todosLosEstados = usuarioService.listaUsuarios();
    }

    public void guardarUsuario() {
        /*Para poder verificar en que parte del sistema se esta*/
        FacesContext context = FacesContext.getCurrentInstance();
        RequestContext requestContext = RequestContext.getCurrentInstance();
        if (usur.getIdUsuario() == null) {
            usuarioService.guardarUsuario(usur);
            limpiarDialogo();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario Guardado", "Nuevo"));
        } else {
            usuarioService.editarUsuario(usur);
            limpiarDialogo();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Usuario Actualizado", "Actualizar"));
            requestContext.execute("PF('dlg_editar_usuario').hide();");
        }
    }

    public void verUsuario(Usuarios usur) {
        this.usur = usur;
    }

    public void eliminarUsuario() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (usur.getIdUsuario() != 0) {
            usuarioService.eliminarUsuario(usur);
            limpiarDialogo();
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Usuario Eliminado", "Eliminar"));
        }
    }

    public void limpiarDialogo() {
        usur = new Usuarios();
        rol = new Roles();
        usur.setRoles(rol);
    }

    public List<Usuarios> usuarioslist() {
        return usuarioService.listaUsuarios();
    }

    public List<Roles> getRolesList() {
        return todoLosRoles = rolesService.listaRoles();
    }
}
